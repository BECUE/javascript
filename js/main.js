// declaration of the variable mydiv (html)
// déclaration de la  variable mydiv (html)
var mydiv = document.querySelector("#mydiv");
// declaration of an empty array variable that will host the JSON data
// déclaration d'une variable tableau vide qui va acceuillir les données JSON
var listPromo = [];
// declaration of an addPromo variable corresponding to the input field (html)
// déclaration d'une variable addPromo qui correspond au champs input (html)
var addPromo = document.querySelector("#addPromo");
// declaration of a variable that corresponds to the Add promo button (html)
// déclaration d'une variable qui correspond au bouton Ajouter promo (html)
var btnAddPromo = document.querySelector("#btnAddPromo");
// declaration of the variable that corresponds to the modify button of the drop-down list (html)
// déclaration de la variable qui correspond au bouton modifier de la liste déroulante (html)
var btnModifyPromo = document.querySelector ("#btnModifyPromo");
// declare the variable that corresponds to the delete button in the dropdown list (html)
//déclaration de la variable qui correspond au bouton supprimer de la liste deroulante (html)
var btnDeletePromo = document.querySelector("#btnDeletePromo");
// declaration of the variable for deletion of the promo
//déclaration de la variable pour la suppression de la promo 
var SelectProm = document.querySelector("#inputState");
// declaration of the variable to display the list of students
// déclaration de la variable pour afficher la liste des éléves
var listStudent = document.querySelector("#listStudent");

// we get the API on the link http://api-students.popschool-lens.fr/api/promotions
// on récupère l'api sur le  lien http://api-students.popschool-lens.fr/api/promotions
function getpromotion(){
fetch("http://api-students.popschool-lens.fr/api/promotions")
    // API recovered by the "response" to be transformed into JSON
   // api récupéré par la "response" pour être transformer en JSON
   .then(response => response.json())
   // we get the JSON with the .then and then put it in the promo
   //  on récupère le JSON avec le .then pour en suite le mettre dans la promo
   .then(function(promotions) {
        // console.log(promo); permet simplement de récupérer l'intégralité de L'API, alors que ce qui nous intéresse n'est que le tableau HYDRA:MEMBER
        console.log(promotions["hydra:member"]);
        //
        listPromotions = promotions["hydra:member"];
        //console.log(listPromo);
        // permet d'afficher la nouvelle liste
        SelectProm.innerHTML='';
        //
        listPromotions.forEach(function(promotion) {
            var promotionOption = document.createElement("option");
            promotionOption.value = promotion.id;
            promotionOption.innerHTML = promotion.name;
            SelectProm.appendChild(promotionOption);
        });
   });
}
getpromotion();
// Create an event listener
// Créer un écouteur d'evenement
btnAddPromo.addEventListener(`click`,createPromotion);
// function that created the new promo
// fonction qui créé la nouvelle promo
function createPromotion (event) {
    fetch("http://api-students.popschool-lens.fr/api/promotions", {
        //
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
            },
            // we use the post method on the fetch to add new promos in the existing chain
            // on utilise la méthode post sur le fetch pour rajouter des nouvelles promos dans la chaine existante
        method: "POST",
        // we convert the value of the input newPromo field to Json string via stringify
        // on transforme la valeur du champs input newPromo en chaine Json via stringify
        body: JSON.stringify({
            name: addPromo.value
        })
    })
    .then(response => response.json())
    .then(promo => {
        //console.log(promo.name + " créé")
        getpromotion();
    })
        
}
// I create an event listener on my button with the function in parameter
// Je crée un event listener sur mon bouton avec la fonction en parametre
btnDeletePromo.addEventListener('click', function () {
    let SelectProm = document.querySelector('#inputState')
    // request confirmation to the user before deletion
    // demande confirmation à l'utilisateur avant suppression 
    if (confirm("Supprimer la promo : " + SelectProm.value + " ?")) {
        // User confirms deletion
        // Utilsateur confirme la suppression
        deletePromotion(SelectProm.value);
    }
})
// function to remove the promo
// fonction pour supprimer la promo
function deletePromotion(idPromo) 
{
        fetch("http://api-students.popschool-lens.fr/api/promotions/" + idPromo, {
            method: "DELETE"
        })
            .then(function (response) {
                getpromotion();
            });
}
// I create an event listener on the button with an anonymous function
// Je crée un event listener sur le bouton avec une fonction anonyme
btnModifyPromo.addEventListener('click', function () {
    let SelectProm = document.querySelector('#inputState')
    // The anonymous function first asks the user to confirm before modification
    // La fonction anonyme demande dabord confirmation à l'utilisateur avant modification 
    if (confirm("Remplacer la promo : " + SelectProm.value + " ?")) {
        // Ensuite elle appelle la fonction modifyPromotion pour réaliser la modification en remplaçant le texte
        modifyPromotion(SelectProm.value);
    }
})
// function to change the name of a promo
// fonction pour modifier le nom d'une promo
function modifyPromotion(idPromo) {
    fetch("http://api-students.popschool-lens.fr/api/promotions/"+ idPromo ,{
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        // we use the put method on the fetch to modify the existing string
        // on utilise la méthode put sur le fetch pour modifier la chaine existante
        method: "PUT",
        // we convert the value of the input newPromo field to Json string via stringify
        // on transforme la valeur du champs input newPromo en chaine Json via stringify
        body: JSON.stringify({
            name: addPromo.value
        })
    })
        .then(response => response.json())
        .then(promo => {
            console.log(promo.name + "modifié")
            getpromotion();
        })
        .catch()
}
// create an event on the button display the list of students
// créer un evenement sur le bouton afficher la liste des étudiants
btnlistStudent.addEventListener(`click`, function(){
    loadStudent(SelectProm.value);
});
// function to display the list of students
// fonction pour afficher la liste des étudiants
function loadStudent(idPromo){
    // fetch for the promo
    // le fetch pour la promo
fetch("http://api-students.popschool-lens.fr/api/promotions/" + idPromo)
    .then(r => r.json())
    .then(function(promo) { 
        listStudent.innerHTML = '';
        promo.students.forEach(function(studentURL) { 
            // fetch for students
            // le fetch pour les éléves
            fetch("http://api-students.popschool-lens.fr" + studentURL)
            .then(r => r.json())
            .then(function(student) {  
                //  variable to display the list of students
                // variable pour afficher la liste des éléves
            var studentCards = createCardStudent(student);
            listStudent.appendChild(studentCards);
            }) 
        });
    });
}
// function to create the dev of a student
// fonction pour créer la dev d'un éléves
function createCardStudent(student) {
   var CardStudent = document.createElement("div");
     var title = document.createElement("h2");
     title.innerHTML = `${student.firstname} ${student.lastname}` ;
     var birthday = document.createElement("p");
     birthday.innerHTML = `${student.birthday}`;

     CardStudent.appendChild(title);
     CardStudent.appendChild(birthday);
    
      return CardStudent ;
 }